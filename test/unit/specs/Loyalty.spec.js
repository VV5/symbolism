import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Loyalty from '@/components/Loyalty'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Loyalty', () => {
  let wrapper, store, state

  beforeEach(() => {
    state = {}

    store = new Vuex.Store({
      state
    })

    wrapper = shallowMount(Loyalty, {
      store, localVue
    })
  })

  test('should next when button is clicked', () => {
    expect(wrapper.vm.storyCounter).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.storyCounter).toBe(1)
  })

  test('should back when button is clicked', () => {
    wrapper.setData({ storyCounter: 1 })

    wrapper.find('.is-back').trigger('click')

    expect(wrapper.vm.storyCounter).toBe(0)
  })
})
