import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Hope from '@/components/Hope'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Hope', () => {
  let wrapper, data, computed

  beforeEach(() => {
    data = {
      storyCounter: 0,
      items: [
        {
          id: 1,
          titles: [
            { name: 'Hello' },
            { name: 'World!' }
          ],
          description: 'Hello World!'
        },
        { id: 2, title: 'Foo', description: 'Bar' }
      ]
    }

    computed = {
      hope: () => data.items[data.storyCounter]
    }

    wrapper = shallowMount(Hope, {
      computed
    })
  })

  test('should go next when button is clicked', () => {
    expect(wrapper.vm.storyCounter).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.storyCounter).toBe(1)
  })

  test('should go next when button is clicked', () => {
    wrapper.setData({ storyCounter: 1 })

    expect(wrapper.vm.storyCounter).toBe(1)

    wrapper.find('.is-back').trigger('click')

    expect(wrapper.vm.storyCounter).toBe(0)
  })

  test('should navigate to activity when button is clicked', () => {
    const $route = {
      path: '/activity'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Hope, {
      router, localVue
    })

    wrapper.setData({ storyCounter: 6 })

    wrapper.find('.goto-activity').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
