import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Activity from '@/components/Activity'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Activity', () => {
  let wrapper, store, state

  beforeEach(() => {
    state = {
      symbols: []
    }

    store = new Vuex.Store({
      state
    })

    wrapper = shallowMount(Activity, {
      attachToDocument: true,
      store,
      localVue
    })
  })

  test('', () => {

  })
})
