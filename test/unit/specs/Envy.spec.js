import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Envy from '@/components/Envy'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Envy', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Envy, {
      attachToDocument: true
    })

    window.HTMLMediaElement.prototype.pause = () => {}
  })

  test('should go next when button is clicked', () => {
    expect(wrapper.vm.storyCounter).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.storyCounter).toBe(1)
  })

  test('should go back when button is clicked', () => {
    wrapper.setData({ storyCounter: 1 })

    expect(wrapper.vm.storyCounter).toBe(1)

    wrapper.find('.is-back').trigger('click')

    expect(wrapper.vm.storyCounter).toBe(0)
  })

  test('should navigate to activity when button is clicked', () => {
    const $route = {
      path: '/activity'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Envy, {
      router, localVue
    })

    wrapper.setData({ storyCounter: wrapper.vm.items.length - 1 })

    wrapper.find('.goto-activity').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
