import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import App from '@/App'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('App', () => {
  let store, state, mutations

  beforeEach(() => {
    state = {}

    mutations = {
      isLandscape: jest.fn(),
      setVersion: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    shallowMount(App, {
      attachToDocument: true,
      store,
      localVue
    })
  })

  test('defaults isLandscape to true', () => {
    expect(mutations.isLandscape).toHaveBeenCalled()
  })
})
