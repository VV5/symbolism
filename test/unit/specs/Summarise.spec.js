import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Summarise from '@/components/Summarise'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Summarise', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Summarise, {
      localVue
    })
  })

  test('should navigate back to symbols selection page when button is clicked', () => {
    const $route = {
      path: '/activity'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Summarise, {
      router, localVue
    })

    wrapper.find('.goto-activity').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
