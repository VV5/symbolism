import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Peace from '@/components/Peace'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Peace', () => {
  let wrapper, store, state, mutations

  beforeEach(() => {
    state = {
      matchedItem: {
        image: 'symbol-peace.png'
      }
    }

    store = new Vuex.Store({
      state, mutations
    })

    wrapper = shallowMount(Peace, {
      store, localVue
    })
  })

  test('should go next when button is clicked', () => {
    expect(wrapper.vm.storyCounter).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.storyCounter).toBe(1)
  })

  test('should go back when button is clicked', () => {
    wrapper.setData({ storyCounter: 1 })

    expect(wrapper.vm.storyCounter).toBe(1)

    wrapper.find('.is-back').trigger('click')

    expect(wrapper.vm.storyCounter).toBe(0)
  })

  test('should navigate to activity when button is clicked', () => {
    const $route = {
      path: '/activity'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Peace, {
      store, router, localVue
    })

    wrapper.setData({ storyCounter: 3 })

    wrapper.find('.goto-activity').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
