import Vue from 'vue'
import Router from 'vue-router'
import Introduction from '@/components/Introduction'
import Activity from '@/components/Activity'
import Peace from '@/components/Peace'
import Hope from '@/components/Hope'
import Loyalty from '@/components/Loyalty'
import Envy from '@/components/Envy'
import Summarise from '@/components/Summarise'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Introduction',
      component: Introduction,
      meta: {
        title: 'Introduction'
      }
    },
    {
      path: '/activity',
      name: 'Activity',
      component: Activity,
      meta: {
        title: 'Activity'
      }
    },
    {
      path: '/peace',
      name: 'Peace',
      component: Peace,
      meta: {
        title: 'Peace'
      }
    },
    {
      path: '/hope',
      name: 'Hope',
      component: Hope,
      meta: {
        title: 'Hope'
      }
    },
    {
      path: '/loyalty',
      name: 'Loyalty',
      component: Loyalty,
      meta: {
        title: 'Loyalty'
      }
    },
    {
      path: '/envy',
      name: 'Envy',
      component: Envy,
      meta: {
        title: 'Envy'
      }
    },
    {
      path: '/summarise',
      name: 'Summarise',
      component: Summarise,
      meta: {
        title: 'Summarise'
      }
    }
  ]
})
