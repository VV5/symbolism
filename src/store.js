import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    isLandscape: true,
    symbols: [
      {
        id: 1,
        name: 'Peace',
        title: 'Gandhi',
        match: 4,
        image: 'symbol-peace.png',
        backgroundColor: '#fff045',
        color: '#3b7bac',
        selected: false,
        completed: false,
        disabled: false
      },
      {
        id: 2,
        name: 'Hope, Celebration & Remembrance',
        title: 'Candle',
        match: 3,
        image: 'symbol-hope.png',
        backgroundColor: '#3b7bac',
        color: '#fff045',
        selected: false,
        completed: false,
        disabled: false
      },
      {
        id: 3,
        name: 'Loyalty',
        title: 'Dog',
        match: 2,
        image: 'symbol-loyalty.png',
        hrefImage: 'dog-screen.png',
        backgroundColor: '#fff045',
        color: '#3b7bac',
        selected: false,
        completed: false,
        disabled: false,
        story: 'To find out more about the amazing loyalty demonstrated by dogs in real life, click on the newspaper to read the story of 2 family dogs in the United States, that refused to leave the side of a dying baby girl.',
        link: 'https://www.straitstimes.com/world/europe/family-dogs-refuse-to-leave-side-of-dying-us-baby-girl'
      },
      {
        id: 4,
        name: 'Envy',
        title: 'Green',
        match: 1,
        image: 'symbol-envy.png',
        backgroundColor: '#3b7bac',
        color: '#fff045',
        selected: false,
        completed: false,
        disabled: false
      }
    ],
    meanings: [
      {
        id: 1,
        name: 'Envy',
        shortName: 'envy',
        button: false
      },
      {
        id: 2,
        name: 'Loyalty',
        shortName: 'loyalty',
        button: false
      },
      {
        id: 3,
        name: 'Hope, Celebration' + '\n' + '& Remembrance',
        shortName: 'hope',
        button: false
      },
      {
        id: 4,
        name: 'Peace',
        shortName: 'peace',
        button: false
      }
    ],
    matchedItem: null,
    matchedCount: [],
    isCompleted: 0,
    boxSpace: 0,
    showSummaryButton: false
  },
  mutations: {
    isLandscape (state, status) {
      state.isLandscape = status
    },
    setBoxSpace (state) {
      state.boxSpace++
    },
    checkingCompleted (state) {
      state.isCompleted += 1
    },
    setMatchedItem (state, item) {
      state.matchedItem = item

      const index = state.symbols.indexOf(item)
      state.symbols[index].selected = true
    },
    setItemDisabled (state, items) {
      items.forEach(item => {
        state.symbols[item - 1].disabled = true
      })
    },
    incrementMatchedCount (state, id) {
      state.matchedCount.push(id)
    },
    setItemEnabled (state, items) {
      items.forEach(item => {
        state.symbols[item - 1].disabled = false
      })
    },
    setCompleted (state, matchedItem) {
      const index = state.symbols.indexOf(matchedItem)

      state.symbols[index].completed = true
    },
    showButton (state, item) {
      switch (item) {
        case 1:
          state.meanings[3].button = true
          break
        case 2:
          state.meanings[2].button = true
          break
        case 3:
          state.meanings[1].button = true
          break
        case 4:
          state.meanings[0].button = true
          break
        default:
          break
      }
    },
    checkingComplete (state) {
      const isCompleted = state.symbols.every(symbol => symbol.completed === true)

      if (isCompleted) state.showSummaryButton = true
    },
    resetGame (state) {
      state.matchedItem = null
      state.matchedCount = []
      state.sCompleted = 0
      state.boxSpace = 0
      state.showSummaryButton = false
    }
  },
  getters: {
    disabledItem (state) {
      return state.symbols.some(symbol => symbol.disabled === true)
    }
  }
})
