import Vue from 'vue'
import App from './App'
import store from './store'
import router from './router'
import '@mdi/font/scss/materialdesignicons.scss'

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  document.title = `${to.meta.title} | Symbolism`
  next()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: {App}
})
