# Changelog

## v2.2.1
- Updated packages with breaking changes

## v2.2.0
- Upgraded to `vue-loader` v15
- Removed version value in Vuex state
- Fixed page reset on resize
- Updated assets
- Optimised webpack configuration for production
- Fixed items not matched correctly
- Removed unnecessary `async/await`
- Upgraded to Webpack v4
- Removed e2e runner stuff

## 1.0.1
- Removed beta tag
- Cleanup and re-format

## 1.0.0
Initial release

- Added all assets and necessary styling
- Added audio
- Added material icons
- Added a function to disable dragging after an item is matched
- Fixed incorrect matching boxes position
- Fixed issue with refresh on production
